# Snow Wars
## Version 0.8
## Created by Celeste Privitera (@Xecestel)
## Licensed Under GPL 3 (see below)

This is a 2D pixel art party-game about snowball fights!

# Licenses

Snow Wars
Copyright (C) 2019  Celeste Privitera

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


The graphic assets made by Xecestel (Celeste Privitera) are licensed under the Creative Commons Attribution-ShareAlike 4.0 Unported License.
To view a copy of this license, visit [this page](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
or read "LICENSE_CC-BY-SA.txt" in this directory.

Said assets are:
- "Assets/Icons" directories content.

Some audio assets are not made specifically for this game and are part of "The Essential Retro Video Game Sound Effects Collection" by Juhani Junkala and are licensed under [CC0 Creative Commons License](https://creativecommons.org/publicdomain/zero/1.0/deed.it).

Said assets are:
- "Assets/Audio/Sound Effects/CreativeCommons" directory content.

<!--This game contains also third party music, such as:-->

And third party graphics, such as:

- The "Assets/Backgrounds/Background" folder (original title: *Winter Background, parexel scrolling*) created by [pechvogel](https://opengameart.org/users/pechvogel) (License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/))
- "MenuBackground" is a derivative work created by Xecestel from *Winter Wonderland Pack* created by [Molly "Cougarmint" Willits](https://opengameart.org/users/cougarmint) (License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/))
- The "Assets/Backgrounds/Winter backgrounds" folder (original title: *Winter Background*) created by [pechvogel](https://opengameart.org/users/pechvogel) (License: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/))


Third parties assets are licensed under licenses chose by their author, you can find more info in the respective directories.

This include:
- "Assets/Fonts/OpenDyslexic", "Assets/Fonts/snowtop-caps" and "Assets/Fonts/Sary" directories.


The "LICENSE" file in this directory refers to all parts of the code made for this game by Xecestel (Celeste Privitera), unless otherwhise stated somewhere inside the script directory.

# Changelog

### Version 0.1

- Started implementation of Title Screen.

### Version 0.2

- Added Credits scene and text

### Version 0.2.2

- Added icons

### Version 0.3

- Started working on Options Menu
- Added OpenDyslexic font and resource
- Added SaveLoad script and improved GlobalVariables (added save and load features)
- Added Sound Manager Plugin

### Version 0.4
- Added Menu Background Music
- Updated Sound Manager Plugin to last version
- Updated Localization file
- Added Play Menu on Title Screen

### Version 0.5
- Options Menu code is almost finished
- Updated Localization File

### Version 0.6
- Added gamma correction
- Improved code readability

### Version 0.7
- Added Character class
- Added characters handling on GlobalVariables script
- Started working on Character Selection screen

### Version 0.8
- Improved Character Select screen
- Added PlayersManager
- Improved readability

### Version 0.8.1
- Added CharacterStatBars
- Updated localization
