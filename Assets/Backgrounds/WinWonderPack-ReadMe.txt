Credit: Molly "Cougarmint" Willits
Created by hand using Microsoft Paint and Photoshop Educational Edition 6.0.

Licence: CC-BY 3.0.

Free Commercial Use: Yes
Free Personal Use: Yes

Included in this Pack:
Snowbackground320x240.png
Snowbackground640x480.png
Snowbackground1024x768.png

Extras Folder:
snowflakesingle.png
snowflake_lrg.png
tree.png
tree_lrg.png
tree_snow.png
tree_snow_lrg.png

Donations: Not needed, but appreciated. Contact me if you'd like to make a donation.