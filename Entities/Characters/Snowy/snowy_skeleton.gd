extends Skeleton2D

# Variables

var direction : float = 1 : set = set_direction

@onready var animation = $AnimationPlayer as AnimationPlayer

################

func play_animation(anim_name : String) -> void:
	if animation.has_animation(anim_name):
		animation.play(anim_name)


func set_direction(dir : float) -> void:
	direction = dir
	if direction > 0:
		scale.x = 1
	elif direction < 0:
		scale.x = -1
