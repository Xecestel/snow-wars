extends CharacterBody2D

# Constants

const SPEED = 300.0

#############

# Variables

var motion := Vector2.ZERO

@onready var sprite = $Sprite2D
@onready var particles = $GPUParticles2D

###############

func _physics_process(delta : float) -> void:
	motion.move_toward(Vector2.ZERO, delta)
	_check_collision(move_and_collide(SPEED * motion.normalized() * delta))


func _check_collision(collision : KinematicCollision2D) -> void:
	if not collision:
		return
	
	sprite.visible = false
	particles.emitting = true
	set_physics_process(false)


###### Signals

func _on_gpu_particles_2d_finished() -> void:
	queue_free()
