extends CharacterBody2D

# Constants

const SPEED = 300.0
const JUMP_VELOCITY = -400.0
const BASE_RANGE = 500

const SNOWBALL_SCENE : String = "res://Entities/Snowball/snowball.tscn"

##############

# Variables

var hp : int = 100

var direction : int = 1

@export var player_id = 0

@export var character : Character = null

@onready var shooting_direction = $ShootingDirection as RayCast2D
@onready var line = $Line2D as Line2D
@onready var state_machine = $StateMachine as StateMachine
@onready var state_label = $Label as Label
@onready var skeleton = $Skeleton

##############

# Signals

signal snowball_thrown(pos, target, player)

############

func _ready() -> void:
	state_machine.init(self)
	collision_layer = player_id + 1
	if character:
		hp = hp * character.def / 2
		state_machine.set_speed(float(character.agl) / 2)


func play_animation(anim_name : String) -> void:
	skeleton.play_animation(anim_name)


func set_direction(direction : float) -> void:
	skeleton.direction = direction


func _process(delta : float) -> void:
	state_machine.process_frame(delta)


func _input(event : InputEvent) -> void:
	state_machine.process_input(event, player_id)
	manage_aim()
	if Input.is_action_just_pressed("shoot_" + str(player_id)):
		shoot()


func get_range() -> float:
	return BASE_RANGE * character.str / 2


func shoot() -> void:
	emit_signal("snowball_thrown", global_position, shooting_direction.target_position, player_id)


func manage_aim() -> void:
	var direction = Input.get_vector("aim_right_" + str(player_id), "aim_left_" + str(player_id), "aim_up_" + str(player_id), "aim_down_" + str(player_id))
	
	if direction == Vector2.ZERO:
		return
	
	direction = direction.normalized()
	shooting_direction.target_position.x = -direction.x * get_range()
	shooting_direction.target_position.y = direction.y * get_range()
	shooting_direction.target_position.normalized()
	
	line.clear_points()
	line.add_point(shooting_direction.position)
	line.add_point(shooting_direction.target_position)


func _physics_process(delta : float) -> void:
	state_machine.process_physics(delta, player_id)



func show_state(state_name : String) -> void:
	state_label.text = state_name.capitalize()
