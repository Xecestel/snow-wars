extends CharacterBody2D

#constants#
const UP = Vector2.UP;
const MAX_SPEED = 350;
const GRAVITY = 10;
const STAT_COEFFICIENT = 3; #used to normalize the value of the stats
#const SKIN_DIRECTORY_PATH = "res://Entities/Player/";
#const SCREEN_SHAKE_AMPLITUDE = 2.5;
#const MAX_SCREEN_SHAKES = 10;
##########

#variables#
var player_id : int = 0;
var character_name : String;
var Stats : Dictionary = {
	"Weight"			: 1,
	"Damage"			: 1,
	"Recharge Speed"	: 1,
	"Range"				: 1,
	"Fire Rate"			: 1,
	"Ammo"				: 1
	};

var MOTION = Vector2.ZERO;
var MAX_JUMP_HEIGHT = GlobalVariables.get_unit_y() * 3 * (STAT_COEFFICIENT/self.get_weight());
var MIN_JUMP_HEIGHT = MAX_JUMP_HEIGHT / 2;
var ACCELERATION = 100 * (3/self.get_weight());
var weapon = null;
var is_jumping = false;
var is_falling = false;
var friction = true;

@onready var shooting_direction_raycast = self.get_node("ShootingDirection");
@onready var direction_raycast = self.get_node("Direction");
@onready var collision_shape = self.get_node("CollisionShape2D");
###########


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	self.check_mouse_vector();
	self._handle_move_input();
	self.execute_movement();
	
func execute_movement() -> void:
	var snap = Vector2.DOWN * 32 if !is_jumping else Vector2.ZERO;

	if (self.is_jumping && self.MOTION.y >= 0):
		self.is_jumping = false;
	if (!self.is_on_floor() && self.MOTION.y >= 0):
		self.is_falling = true;
	if (self.is_falling && self.is_on_floor()):
		self.is_falling = false;
		$AnimationPlayer.play("fall");

	if (friction == true):
		MOTION.x = lerp(MOTION.x, 0, 0.05);
	self.set_velocity(MOTION)
	# TODOConverter3To4 looks that snap in Godot 4 is float, not vector like in Godot 3 - previous value `snap`
	self.set_up_direction(UP)
	self.move_and_slide()
	MOTION = self.velocity;

#########################
#	INPUT HANDLING		#
#########################
func check_mouse_vector() -> void:
	var mouse_pos = Vector2.ZERO;
	var distance = mouse_pos - self.position;
	self.shooting_direction_raycast.target_position = distance.normalized();

func _handle_shoot_input() -> void:
	#if (Input.is_action_just_pressed("shoot")):
	#	self.shoot_snowball();
	pass

func _handle_move_input() -> void:
	pass

func _apply_gravity(delta):
	self.MOTION.y += GRAVITY;

func _apply_movement() -> void:
	pass


#########################
#	SETTERS & GETTERS	#
#########################

func get_weight() -> int:
	return self.Stats["Weight"];

func get_damage() -> int:
	return self.Stats["Damage"];

func get_recharge_speed() -> int:
	return self.Stats["Recharge Speed"];

func get_range() -> int:
	return self.Stats["Range"];

func get_fire_rate() -> int:
	return self.Stats["Fire Rate"];

func get_ammo() -> int:
	return self.Stats["Ammo"];

func set_character_name(new_name : String) -> void:
	self.character_name = new_name;

func get_character_name() -> String:
	return self.character_name;

func set_stats(Values : Dictionary) -> void:
	self.character_name = Values["Character Name"];
	for stat in Stats.keys():
		Stats[stat] = Values[stat];
