#extends Node
#
## Declare member variables here. Examples:
## var a = 2
## var b = "text"
#
## Called when the node enters the scene tree for the first time.
#func _ready() -> void:
	#self.add_state("idle");
	#self.add_state("run");
	#self.add_state("jump");
	#self.add_state("fall");
	#call_deferred("set_state", States.idle);
#
#func _input(event) -> void:
	##Checks if the player is currently running or idleing
	#if ([States.idle, States.run].has(state)):
		#if (event.is_action_pressed("jump")):
			#if (Input.is_action_pressed("down")):
				#pass
		#else:
			#parent.MOTION.y = parent.MAX_JUMP_HEIGHT;
			#parent.is_jumping = true;
	#if (state == States.jump):
		#if (event.is_action_released("jump") && parent.MOTION.y < parent.MIN_JUMP_HEIGHT):
			#parent.MOTION.y = parent.MIN_JUMP_HEIGHT;
	#
##This method will handle the logic of each state
#func _state_logic(delta) -> void:
	#parent._handle_move_input();
	#parent._apply_gravity(delta);
	#parent._apply_movement();
#
##This method will handle the transition between different states
#func _get_transition(delta, default_return_value : int = -1) -> int:
	#match state:
		#States.idle:
			#if (!parent.is_on_floor()):
				#if (parent.MOTION.y < 0):
					#return States.jump;
				#elif (parent.MOTION.y >= 0):
					#return States.fall;
			#elif (parent.MOTION.x != 0):
				#return States.run;
		#States.run:
			#if (!parent.is_on_floor()):
				#if (parent.MOTION.y < 0):
					#return States.jump;
				#elif (parent.MOTION.y >= 0):
					#return States.fall;
			#elif (parent.MOTION.x == 0):
				#return States.idle;
		#States.jump:
			#if (parent.is_on_floor()):
				#return States.idle;
			#elif (parent.MOTION.y >= 0):
				#return States.fall;
		#States.fall:
			#if (parent.is_on_floor()):
				#return States.idle;
			#elif (parent.MOTION.y < 0):
				#return States.jump;
	#return default_return_value;
#
##This method will make the state machine enter a new state
#func _enter_state(new_state, old_state) -> void:
	#match new_state:
		#States.idle:
			#print_debug("Idle");
			#pass
		#States.run:
			#print_debug("Run");
			#pass
		#States.jump:
			#print_debug("Jump");
			#pass
		#States.fall:
			#print_debug("Fall");
			#pass
	#
##This method will make the state machine exit an old state
#func _exit_state(old_state, new_state) -> void:
	#pass
