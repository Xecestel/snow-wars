@tool
extends HBoxContainer

# Variables
@export var button_label_text : String = "Button" : set = _set_label_text
@export var button_icon_texture : Texture2D = load("res://icon.png") : set = set_button_texture

# Node variables
@onready var icon = $Icon
@onready var button = $Button
###########

# Signals

signal pressed;

############

func _set_label_text(text : String) -> void:
	if has_node("Button"):
		button_label_text = text
		$Button.text = button_label_text


func set_button_texture(texture : Texture2D) -> void:
	if has_node("Icon"):
		button_icon_texture = texture
		$Icon.texture = button_icon_texture

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	#self.update_font();
	self.icon.set_texture(button_icon_texture);
	self.button.set_text(tr(button_label_text));
	
#func update_font() -> void:
	#button.set("theme_override_fonts/font", GlobalVariables.get_font_to_use());

func _on_Button_pressed() -> void:
	emit_signal("pressed");


func _on_Button_mouse_entered():
	grab_focus();


func _on_button_pressed():
	emit_signal("pressed")
