extends VBoxContainer

#variables#
var character : Character;

#Node variables#
#onready var icon_rect = self.get_node("Icon");
#onready var name_label = self.get_node("Name");
###########

#singals#
signal pressed;
#########

# Called when the node enters the scene tree for the first time.
func _ready():
	self.update_font();

func connect_signals(root : Node) -> void:
	self.connect("pressed", Callable(root, "_on_CharacterButton_pressed"));
	self.connect("focus_entered", Callable(root, "_on_CharacterButton_focus_entered").bind(self));

func set_character(new_char : Character) -> void:
	character = new_char;

func get_character() -> Character:
	return character;

func update_gui() -> void:
	$Icon.set_texture(character.get_icon());
	$Name.set_text(character.get_name());

func update_font() -> void:
	var font_to_use = GlobalVariables.get_font_to_use();
	$Name.set("theme_override_fonts/font", font_to_use);
	
func grab_focus() -> void:
	emit_signal("focus_entered", self);
	
func release_focus() -> void:
	pass

func _on_CharacterButton_mouse_entered() -> void:
	self.grab_focus();

func _on_CharacterButton_mouse_exited() -> void:
	self.release_focus();

func _on_CharacterButton_gui_input(event : InputEvent):
	if (event is InputEventMouseButton && Input.is_mouse_button_pressed(1)):
		emit_signal("pressed");
