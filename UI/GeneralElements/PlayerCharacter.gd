@tool
extends NinePatchRect

#variables

@export var player_id := 1 : set = _set_player_id

var selected = false
var joined = false
var character_info : Dictionary

@onready var info_grid = self.get_node("VBoxContainer/CharacterInfo");
@onready var info_grid_nodes = {
	"Character Name"	: $VBoxContainer/CharacterInfo/Name,
	"Defence"			: $VBoxContainer/CharacterInfo/Defence/Value,
	"Strenght"			: $VBoxContainer/CharacterInfo/Strenght/Value,
	"Agility"			: $VBoxContainer/CharacterInfo/Agility/Value
	};
	
###########

# Called when the node enters the scene tree for the first time.
#func _ready() -> void:
	#self.update_fonts();
	
#func update_fonts() -> void:
	#var font_to_use = GlobalVariables.get_font_to_use();
	#for info in info_grid.get_children():
		#if info is HBoxContainer:
			#for child in info.get_children():
				#if child is Label:
					#child.set("theme_override_fonts/font", font_to_use);
		#else:
			#info.set("theme_override_fonts/font", font_to_use);


func _set_player_id(id : int) -> void:
	player_id = id
	$VBoxContainer/CharacterInfo/Name.text = "Player " + str(id)

func set_character_info(info : Dictionary) -> void:
	self.character_info = info;
	self.update_gui();

func set_character_name(new_name : String) -> void:
	info_grid_nodes["Character Name"].set_text(new_name);
	
func update_gui() -> void:
	for key in character_info:
		if (key == "Ammo"):
			info_grid_nodes[key].set_text(str(character_info[key]));
		else:
			info_grid_nodes[key].set_value(character_info[key]);
		
func join(enable : bool) -> void:
	$JoinInstr.set_visible(!enable);
	$VBoxContainer.set_visible(enable);
	self.joined = enable;
	
func has_joined() -> bool:
	return self.joined;
