#extends Node
#
##constants#
#const CHARACTERS_DIR = "res://Data/Characters/"
#const DEFAULT_SETTINGS_FILE = "res://Data/DefaultSettings.json";
#const LANGUAGES = ["en", "it"];
#const UNIT = Vector2(32.0, 32.0);
#const FINAL_BUILD = false;
#const DEBUG = true;
############
#
##variables#
#@onready var FONTS : Dictionary = {
	#"text"				: load("res://Assets/Fonts/text.tres"),
	#"opendyslexic"		: load("res://Assets/Fonts/opendyslexic_text.tres")
	#}
	#
#var Settings : Dictionary; #The settings are loaded from the DefaultSettings file
#var Characters = [];
#############
#
#
## Called when the node enters the scene tree for the first time.
#func _ready():
	#if (SaveLoad.load_game() == false):
		#self.restore_defaults();
		#SaveLoad.save_game();
	#self.load_on_engine();
	#
	##Gets the current default language from the system locale
	#self.set_current_language_id(OS.get_locale().split("_")[0]);
#
	##Loads every character from the characters data directory
	#self.load_characters();
#
#
#func get_unit() -> Vector2:
	#return UNIT;
	#
#func get_unit_x() -> float:
	#return UNIT.x;
	#
#func get_unit_y() -> float:
	#return UNIT.y;
#
#func get_characters_list() -> Array:
	#return Characters;
	#
##################################
##		SETTINGS MANAGEMENT		#
##################################
#
#func restore_defaults() -> void:
	#var defaultsFile = File.new()
	#var filePath = DEFAULT_SETTINGS_FILE;
	#
	#if !defaultsFile.file_exists(filePath):
		#print_debug("File not found at given location: " + filePath);
		#defaultsFile.close();
		#return;
	#defaultsFile.open(filePath, File.READ);
	#
	#var txt = defaultsFile.get_as_text();
	#
	#print_debug("loaded default values " + txt);
	#defaultsFile.close()
	#
	#if txt == null || txt == "":
		#print_debug("No text found");
		#return
		#
	#var test_json_conv = JSON.new()
	#test_json_conv.parse(txt);
	#var results = test_json_conv.get_data()
	#
	#if results.error != OK:
		#print_debug("Error loading defaults values: " + results.error_string)
		#return;
	#
	#Settings = results.result;
##end
#
#func load_on_engine() -> void:
	#self.debug_print("Loading settings on engine...");
	#
	#self.debug_print("Setting if game is mute...");
	#AudioServer.set_bus_mute(0, self.is_mute());
	#
	#self.debug_print("Setting Sound Effects Volume...");
	#var se_db = (self.get_sfx_level() * 104 / 100) - 80;
	#SoundManager.set_se_volume_db(se_db);
	#
	#self.debug_print("Setting Background Music Volume...");
	#var bgm_db = (self.get_bgm_level() * 104 / 100) - 80;
	#SoundManager.set_bgm_volume_db(bgm_db);
	#
	#self.debug_print("Setting Music Effects Volume...");
	#var me_db = (self.get_mfx_level() * 104 / 100) - 80;
	#SoundManager.set_me_volume_db(me_db);
	#
	#self.debug_print("Setting Window Mode...");
	#match Settings["WINDOW"]:
		#0:
			#get_window().borderless = (false);
			#get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN if (false) else Window.MODE_WINDOWED;
		#1:
			#get_window().borderless = (true);
			#get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN if (false) else Window.MODE_WINDOWED;
		#2:
			#get_window().borderless = (false);
			#get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN if (true) else Window.MODE_WINDOWED;
	#
	#self.debug_print("Setting Gamma...");
	#GammaController.set_gamma(Settings["GAMMA"]);
	#
	#self.debug_print("Load on engine complete.");
##end
#
#func set_font_to_use(font : String) -> void:
	#Settings["FONT_TO_USE"] = font;
#
#func get_font_to_use() -> Resource:
	#return FONTS[Settings["FONT_TO_USE"]];
#
#func set_current_language_id(id : String, fallback : int = 0) -> void:
	#var language = LANGUAGES.find(id);
	#self.Settings["LANGUAGE_ID"] = language if language >= 0 else fallback;
#
#func get_current_language_id() -> String:
	#return LANGUAGES[Settings["LANGUAGE_ID"]];
#
#func set_mute(mute : bool) -> void:
	#Settings["MUTE"] = mute;
#
#func is_mute() -> bool:
	#return Settings["MUTE"];
	#
#func set_sfx_level(sfx_level : int) -> void:
	#Settings["SFX"] = sfx_level;
#
#func get_sfx_level() -> int:
	#return Settings["SFX"];
	#
#func set_bgm_level(bgm_level : int) -> void:
	#Settings["BGM"] = bgm_level;
	#
#func get_bgm_level() -> int:
	#return Settings["BGM"];
	#
#func set_mfx_level(mfx_level : int) -> void:
	#Settings["MFX"] = mfx_level;
	#
#func get_mfx_level() -> int:
	#return Settings["MFX"];
#
#func set_gamma(value : int) -> void:
	#Settings["GAMMA"] = value;
	#GammaController.set_gamma(value);
#
#func get_gamma() -> int:
	#return Settings["GAMMA"];
	#
#func set_settings(settings : Dictionary) -> void:
	#Settings = settings.duplicate();
	#self.load_on_engine();
#
#func get_settings() -> Dictionary:
	#return Settings;
#
##################################
##		DATA SAVE AND LOAD		#
##################################
#
#func save():
	#var save_dict = Settings;
	#
	#if (save_dict == null):
		#print_debug("Error retrieving values for the save dictionary");
		#return;
	#
	#print_debug("Game saved correctly");
	#return save_dict;
##end
#
#func load_data(save_file : File) -> bool:
	#if (save_file == null):
		#return false;
		#
	#var saved_game_version = 0.0;
	#var test_json_conv = JSON.new()
	#test_json_conv.parse(save_file.get_line());
	#var current_line = test_json_conv.get_data()
	#
	#self.Settings = current_line;
#
	#return true;
#
##################################
##		INTERNAL METHODS		#
##################################
#
##Prints a string on the debug shell if the game is in DEBUG mode
#func debug_print(string : String) -> void:
	#if (DEBUG):
		#print_debug(string);
	#
##Loads every character from the data directory and stores them in an Array
#func load_characters() -> void:
	#self.debug_print("Loading characters...");
	#var dir = DirAccess.new();
	#var file = File.new();
	#if (dir.open(CHARACTERS_DIR) == OK):
		#dir.list_dir_begin() ;# TODOConverter3To4 fill missing arguments https://github.com/godotengine/godot/pull/40547
		#var file_name = dir.get_next();
		#while (file_name != "" && file_name.get_extension() == "json"):
			#self.debug_print("Adding new character: " + file_name.get_basename());
			#file.open(CHARACTERS_DIR + file_name, File.READ);
			#var test_json_conv = JSON.new()
			#test_json_conv.parse(file.get_as_text()).result;
			#var result = test_json_conv.get_data()
			#file.close();
			#if (typeof(result) == TYPE_DICTIONARY):
				#self.debug_print("Character info: " + str(result));
				#var new_character = Character.new(result);
				#self.Characters.append(new_character);
			#else:
				#self.debug_print("An error occurred while trying to access the character data");
				#return;
			#file_name = dir.get_next();
	#else:
		#self.debug_print("An error occurred while trying to open the characters data directory");
	#dir.list_dir_end();
	#self.debug_print("Loading character complete");
