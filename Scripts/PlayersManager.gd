extends Node;

#constants#
const MAX_PLAYERS = 4;

#variables#
var Players : Dictionary = {};
var left_players = [];
###########

func _ready() -> void:
	Input.connect("joy_connection_changed", Callable(self, "_on_Input_joy_connection_changed"));

#Returns the player id by their device
func get_player(device : int) -> int:
	var index = Players.values().find(device);
	var player = -1;
	if (index < 0):
		GlobalVariables.debug_print("Player not found with device " + str(device));
	else:
		player = Players.keys()[index];
	return player;

#Adds a new player
func add_player(device : int) -> int:
	var number_of_players = Players.size();
	if (number_of_players >= MAX_PLAYERS):
		GlobalVariables.debug_print("Too many players");
		return -1;
	var player;
	if (left_players.is_empty()):
		player = number_of_players + 1;
	else: #If someone has left the game while other players where playing
		player = left_players.pop_front(); #Any new player will get the empty spot
	Players[player] = device;
	GlobalVariables.debug_print("P" + str(player) + " with device " + str(device) + " connected"); 
	return player;

#Removes the player
func remove_player(player_id : int) -> bool:
	var removed = false;
	var player = player_id;
	if (Players.has(player) == false):
		GlobalVariables.debug_print("The player doesn't exist");
	else:
		var device_used = Players[player];
		removed = Players.erase(player);
		if (removed):
			GlobalVariables.debug_print("P" + str(player) + " with device " + str(device_used) + " has been removed");
			left_players.push_back(player);
	return removed;

#Returns true if one of the current players is using the device
func is_device_known(device : int) -> bool:
	return Players.values().has(device);

#########################
#	SIGNALS HANDLING	#
#########################

func _on_Input_joy_connection_changed(device_id : int, connected : bool) -> void:
	if (self.is_device_known(device_id) && connected == false):
		var player = self.get_player(device_id);
		self.remove_player(player);
