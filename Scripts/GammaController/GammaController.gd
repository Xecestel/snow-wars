extends CanvasLayer
#This module allows to manage the in-game gamma correction
#It basically just uses a ColoRect to put a white or black filter on the screen

#variables#
@onready var gamma_node = self.get_node("Gamma");
###########

func set_gamma(value : float) -> void:
	var real_value = value / 100; #value is a percentage
	var color = Color(0, 0, 0, real_value) #if real_value >= 0 else Color(1, 1, 1, abs(real_value));
	gamma_node.set_color(color);