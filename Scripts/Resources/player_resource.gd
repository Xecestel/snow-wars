extends Resource
class_name Character

# Variables

@export var character_name : String = ""
@export var sprite_texture : Texture2D = null
@export_range(1, 4) var agl : int = 1
@export_range(1, 4) var str : int = 1
@export_range(1, 4) var def : int = 1

@export var points : int = 3

#################
