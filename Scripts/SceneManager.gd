extends Node

#variables#
var current_scene = ResourceLoader.load("res://Scenes/TitleScreen/TitleScreen.tscn").instantiate();
###########

#Sets the current scene
func set_current_scene(new_scene : Resource) -> void:
	self.current_scene = new_scene;

#Change the game scene
func change_scene_to_file(scene_path : String) -> void:
	GlobalVariables.debug_print("Changing scene to " + scene_path);
	current_scene.queue_free();
	var new_scene = ResourceLoader.load(scene_path);
	current_scene = new_scene.instantiate();
	get_tree().get_root().add_child(current_scene);
	get_tree().set_current_scene(current_scene);

#Quits the game
func quit_game() -> void:
	self.get_tree().quit();