extends State

# Variables

@export_category("Other states")
@export var fall_state : State
@export var idle_state : State
@export var move_state : State
@export var run_state : State

@export_category("Physics variables")
@export_range(0, 600.0) var jump_force : float = 500.0

################

func enter() -> void:
	super()
	parent.velocity.y = -jump_force


func process_input(event : InputEvent, player_id : int) -> State:
	if Input.is_action_just_pressed('shoot' + "_" + str(player_id)):
		parent.shoot()
	return null


func process_physics(delta : float, player_id : int) -> State:
	parent.velocity.y += gravity * delta
	
	if parent.velocity.y > 0:
		return fall_state
	
	var direction = Input.get_axis('left' + "_" + str(player_id), 'right' + "_" + str(player_id))
	
	if direction != 0.0:
		parent.velocity.x = move_toward(parent.velocity.x, direction * move_speed, friction)
	else:
		parent.velocity.x = move_toward(parent.velocity.x, 0, friction)
	parent.move_and_slide()
	
	if parent.is_on_floor():
		if direction != 0:
			if Input.is_action_pressed("run" + "_" + str(player_id)):
				return run_state
			return move_state
		return idle_state
	return null
