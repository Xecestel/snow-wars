extends State

# Variables

@export_category("Other states")
@export var fall_state: State
@export var jump_state: State
@export var move_state: State
@export var run_state: State

##############################

func enter() -> void:
	super()
	parent.velocity.x = 0

func process_input(event : InputEvent, player_id : int) -> State:
	if Input.is_action_pressed('jump' + "_" + str(player_id)) and parent.is_on_floor():
		return jump_state
	if Input.is_action_pressed('left' + "_" + str(player_id)) or Input.is_action_pressed('right' + "_" + str(player_id)):
		if Input.is_action_pressed("run" + "_" + str(player_id)):
			return run_state
		return move_state
	return null

func process_physics(delta : float, player_id : int) -> State:
	parent.velocity.y += gravity * delta
	parent.move_and_slide()
	
	if not parent.is_on_floor():
		return fall_state
	return null
