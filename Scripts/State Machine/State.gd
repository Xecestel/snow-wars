class_name State
extends Node


@export var animation_name : String
@export_range(0.0, 100.0) var friction = 35.0
@export var move_speed : float = 200

@onready var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

# Hold a reference to the parent so that it can be controlled by the state
var parent : CharacterBody2D

func enter() -> void:
	parent.play_animation(animation_name)
	pass

func exit() -> void:
	pass

func process_input(event : InputEvent, player_id : int) -> State:
	return null	

func process_frame(delta : float) -> State:
	return null

func process_physics(delta : float, player_id : int) -> State:
	return null
