extends State

# Variables

@export_category("Other states")
@export var fall_state : State
@export var idle_state : State
@export var jump_state : State
@export var run_state : State

################

func enter() -> void:
	super()


func process_input(event : InputEvent, player_id : int) -> State:
	if event.is_action_pressed("jump" + "_" + str(player_id)):
		return jump_state
	if event.is_action_pressed("run" + "_" + str(player_id)):
		return run_state
	return null


func process_physics(delta : float, player_id : int) -> State:
	if not parent.is_on_floor():
		return fall_state
	
	# Get the input direction.
	var direction = Input.get_axis("left" + "_" + str(player_id), "right" + "_" + str(player_id))
	
	if direction != 0.0:
		parent.velocity.x = move_toward(parent.velocity.x, direction * move_speed, 20)
	else:
		parent.velocity.x = move_toward(parent.velocity.x, 0, friction)
	parent.move_and_slide()
	parent.set_direction(direction)
	
	if parent.velocity.x == 0.0 and direction == 0.0:
		return idle_state
	
	return null
