extends State

# Variables

@export_category("Other states")
@export var fall_state: State
@export var jump_state: State
@export var move_state: State
@export var idle_state : State
@export var leap_state : State

@export_category("Run state variables")
@export_range(0.0, 50.0) var friction = 50.0

##############################


func enter() -> void:
	super()


func process_input(event : InputEvent) -> State:
	if event.is_action_pressed("jump"):
		if abs(parent.velocity.x) == move_speed:
			return leap_state
		return jump_state
	if event.is_action_released("run"):
		return move_state
	return null


func process_physics(delta : float) -> State:
	if not parent.is_on_floor():
		return fall_state
	
	# Get the input direction.
	var direction = Input.get_axis("left", "right")
	
	if direction != 0.0:
		parent.velocity.x = move_toward(parent.velocity.x, direction * move_speed, 20)
	else:
		parent.velocity.x = move_toward(parent.velocity.x, 0, friction)
	parent.move_and_slide()
	
	if parent.velocity.x == 0.0 and direction == 0.0:
		return idle_state
	
	return null
