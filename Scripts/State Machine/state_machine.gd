extends Node
class_name StateMachine

@export var starting_state : State

var current_state: State
var player = null

# Initialize the state machine by giving each child state a reference to the
# parent object it belongs to and enter the default starting_state.
func init(parent : CharacterBody2D) -> void:
	for child in get_children():
		child.parent = parent
	player = parent

	# Initialize to the default state
	change_state(starting_state)

# Change to the new state by first calling any exit logic on the current state.
func change_state(new_state: State) -> void:
	if current_state:
		current_state.exit()

	current_state = new_state
	current_state.enter()
	player.show_state(current_state.name)
	
# Pass through functions for the Player to call,
# handling state changes as needed.
func process_physics(delta : float, player_id : int) -> void:
	var new_state = current_state.process_physics(delta, player_id)
	if new_state:
		change_state(new_state)

func process_input(event: InputEvent, player_id : int) -> void:
	var new_state = current_state.process_input(event, player_id)
	if new_state:
		change_state(new_state)

func process_frame(delta: float) -> void:
	var new_state = current_state.process_frame(delta)
	if new_state:
		change_state(new_state)


func set_speed(spd : float) -> void:
	for child in get_children():
		child.move_speed *= spd
		child.friction *= spd
