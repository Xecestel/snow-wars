extends State

# Variables

@export_category("Other states")
@export var idle_state : State
@export var move_state : State
@export var run_state : State

######################


# Called when the node enters the scene tree for the first time.
func enter() -> void:
	super()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func process_physics(delta : float, player_id : int) -> State:
	parent.velocity.y += gravity * delta
	
	var direction = Input.get_axis('left' + "_" + str(player_id), 'right' + "_" + str(player_id))
	
	if parent.is_on_floor():
		if direction != 0:
			return move_state
		return idle_state
	
	if direction != 0.0:
		parent.velocity.x = move_toward(parent.velocity.x, direction * move_speed, friction)
	else:
		parent.velocity.x = move_toward(parent.velocity.x, 0, friction)
	parent.move_and_slide()
	
	return null
