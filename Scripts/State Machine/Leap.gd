extends State

# Variables

@export_category("Other states")
@export var fall_state : State
@export var idle_state : State
@export var move_state : State
@export var run_state : State

@export_category("Physics variables")
@export_range(0, 600.0) var jump_force : float = 300.0
@export_range(0, 600.0) var leap_force : float = 1200.0

################

func enter() -> void:
	super()
	parent.velocity.y = -jump_force
	parent.velocity.x += leap_force


func process_input(event : InputEvent) -> State:
	if Input.is_action_just_pressed('attack'):
		parent.attack()
	return null


func process_physics(delta : float) -> State:
	parent.velocity.y += gravity * delta
	
	if parent.velocity.y > 0:
		return fall_state
	
	var movement = Input.get_axis('left', 'right') * move_speed
	
	if movement != 0:
		parent.sprite.flip_h = movement < 0
	parent.velocity.x = movement
	parent.move_and_slide()
	
	if parent.is_on_floor():
		if movement != 0:
			if Input.is_action_pressed("run"):
				return run_state
			return move_state
		return idle_state
	return null
