#extends Node
#class_name StateMachine
#
##variables#
#var state = null;
#var previous_state = null;
#var States : Dictionary = {};
#
#@onready var parent = get_parent();
############
#
#func _physics_process(delta) -> void:
	#if (state != null):
		#self._state_logic(delta);
		#var transition = self._get_transition(delta);
		#if (transition != null):
			#self.set_state(transition);
#
##This is a virtual method that is going to be overridden in the scripts that will inherit it
##This method will handle the logic of each state
#func _state_logic(delta) -> void:
	#pass;
#
##This method will handle the transition between different states
#func _get_transition(delta, default_return_value : int = -1) -> int:
	#return default_return_value;
#
##This method will make the state machine enter a new state
#func _enter_state(new_state, old_state) -> void:
	#pass
	#
##This method will make the state machine exit an old state
#func _exit_state(old_state, new_state) -> void:
	#pass
#
##This will actually make the state machine change between a state and another
#func set_state(new_state):
	#previous_state = state;
	#state = new_state;
	#
	#if (previous_state != null):
		#self._exit_state(previous_state, new_state);
	#if (new_state != null):
		#self._enter_state(new_state, previous_state);
#
##This allows to add new states to the State Machine
#func add_state(state_name):
	#States[state_name] = States.size();
