

#variables#
var name : String;
var animation_sprite : String;
var icon : String;

var Stats : Dictionary;
###########

func _init(info_dictionary : Dictionary) -> void:
	self.name = info_dictionary["Character Name"];
	self.animation_sprite = info_dictionary["Animation Sprite2D"];
	self.icon = info_dictionary["Icon"];
	self.Stats = info_dictionary["Stats"];


#########################
#   SETTERS & GETTERS   #
#########################

func get_name() -> String:
	return self.name;

func get_animation_sprite() -> Resource:
	return super.load(self.animation_sprite);

func get_icon() -> Resource:
	return load(self.icon);

func get_stats() -> Dictionary:
	return Stats;

func get_weight() -> int:
	return Stats["Weight"];

func get_strenght() -> int:
	return Stats["Strenght"];

func get_recharge_speed() -> int:
	return Stats["Recharge_Speed"];

func get_fire_rate() -> int:
	return Stats["Fire Rate"];

func get_max_ammo() -> int:
	return Stats["Ammo"];
