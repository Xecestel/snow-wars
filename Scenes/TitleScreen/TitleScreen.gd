extends Control

#constants#
const CREDIT_SCENE_PATH = "res://Scenes/CreditsScene/Credits.tscn";
const OPTIONS_SCENE_PATH = "res://Scenes/OptionsScene/OptionsMenu.tscn";
const CHARACTER_SELECTION_SCENE_PATH = "res://Scenes/CharacterSelectionScreen/CharacterSelection.tscn";

#variables#
var game_title = ProjectSettings.get_setting("application/config/name");

#Nodes variables
@onready var title = self.get_node("TitleLabel");
@onready var menu_container = self.get_node("MenuContainer");
@onready var play_container = self.get_node("PlayMenuContainer");
@onready var play_button = self.get_node("MenuContainer/PlayBtn");
@onready var options_button = self.get_node("MenuContainer/OptionsBtn");
@onready var credits_button = self.get_node("MenuContainer/CreditsBtn");
@onready var gitlab_button = self.get_node("GitLabBtn");
@onready var local_multiplayer_button = self.get_node("PlayMenuContainer/LocalBtn");
@onready var online_multiplayer_button = self.get_node("PlayMenuContainer/OnlineBtn");
@onready var back_button = self.get_node("PlayMenuContainer/BackBtn");
@onready var copyright_label = $CopyrightLabel

############

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SoundManager.play_bgm("MainMenu");
	#self.update_font();
	title.set_text(game_title);
	play_button.grab_focus();

func _process(delta : float) -> void:
	if (Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_down")):
		SoundManager.play_sfx("UIMove", false);
	if (play_container.is_visible() && Input.is_action_just_pressed("ui_cancel")):
		self.go_back();
	if (Input.is_action_just_pressed("ui_accept")):
		SoundManager.play_sfx("UISelect", false);
	
func update_font() -> void:
	copyright_label.set("theme_override_fonts/font", GlobalVariables.get_font_to_use());

func switch_containers() -> void:
	menu_container.set_visible(!menu_container.is_visible());
	play_container.set_visible(!menu_container.is_visible());

func go_back() -> void:
	self.switch_containers();
	play_button.grab_focus();
	SoundManager.play_se("UIBack", false);
	
#########################
#	SIGNALS HANDLING	#
#########################

#Main Menu Buttons#
func _on_PlayBtn_pressed():
	self.switch_containers();
	local_multiplayer_button.grab_focus();
	SoundManager.play_sfx("UISelect")

func _on_OptionsBtn_pressed():
	SceneManager.change_scene_to_file(OPTIONS_SCENE_PATH);
	SoundManager.play_sfx("UISelect")

func _on_CreditsBtn_pressed():
	SceneManager.change_scene_to_file(CREDIT_SCENE_PATH);
	SoundManager.play_sfx("UISelect")

func _on_ExitBtn_pressed():
	SoundManager.play_sfx("UISelect")
	SceneManager.quit_game();

#Play Meny Buttons#
func _on_LocalBtn_pressed():
	SceneManager.change_scene_to_file(CHARACTER_SELECTION_SCENE_PATH);

func _on_OnlineBtn_pressed():
	SceneManager.change_scene_to_file(CHARACTER_SELECTION_SCENE_PATH);

func _on_BackBtn_pressed():
	self.go_back();

#Other Buttons#
func _on_GitLabBtn_pressed():
	if (OS.shell_open("https://gitlab.com/Xecestel/snow-wars") != OK):
		print("Error opening given URL");

func _on_GitLabBtn_focus_entered():
	self.gitlab_button.self_modulate = Color(1, 1, 1, 0.490196);

func _on_GitLabBtn_focus_exited():
	self.gitlab_button.self_modulate = Color(1, 1, 1);
