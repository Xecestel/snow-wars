extends Control

#constants#
const TITLE_ID	= "CREDITS";
const BACK_ID	= "BACK";

const MAIN_MENU_PATH	= "res://UI/MainMenu/MainMenu.tscn"
##########

#variables#
@onready var transitionMgr = $TransitionMgr;
@onready var file_display = $ColorRect/RichTextLabel;
var back = false;
var current_line = 0;
###########


func _ready():
	$ColorRect/backBtn.grab_focus();
#	$ColorRect/PgUp.text = ControlMap.get_keyboard_button_as_string("ui_pageup");
#	$ColorRect/PgDown.text = ControlMap.get_keyboard_button_as_string("ui_pagedown");
	
func _input(event):
	if (event.is_action_pressed("ui_cancel")):
		#SoundManager.play_se("MenuCancel");
		self.go_back();
	if (event.is_action_pressed("ui_select")):
		self._on_backBtn_pressed();
	if (event.is_action_pressed("ui_page_down")):
		self.scroll_text(1);
	if (event.is_action_pressed("ui_page_up")):
		self.scroll_text(-1);
		
func localization() -> void:
	$CreditsLbl.text	= tr(TITLE_ID);
	$backBtn.text		= tr(BACK_ID);
	
		
func go_back() -> void:
	SceneManager.change_scene_to_file("res://Scenes/TitleScreen/TitleScreen.tscn");
	
func scroll_text(number_of_lines : int) -> void:
	current_line += number_of_lines;
	file_display.scroll_to_line(current_line);

func _on_backBtn_pressed():
	#SoundManager.play_se("MenuSelect");
	self.go_back();
