extends RichTextLabel

@export var fileDir = "" # (String, DIR)
@export var fileName = "" # (String, FILE, "*.txt")

func _ready():
	self.set_bbcode(self.readFile());

func readFile() -> String:
	if (fileDir == "" || fileDir == null ||
		fileName == "" || fileName == null):
		return ""
	var filePath = fileDir + "/" + fileName + "_" + GlobalVariables.get_current_language_id() + ".txt";
	var f = File.new()
	if !f.file_exists(filePath):
		return "file does not exist: '" + str(filePath) + "'"
	f.open(filePath, File.READ)
	var text = f.get_as_text()
	f.close()
	return text
