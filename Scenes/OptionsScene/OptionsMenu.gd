extends Control

#constants#
const BACK_SCENE = "res://Scenes/TitleScreen/TitleScreen.tscn";

#variables#

#The Settings are copied from the GlobalVariables.
#The script uses its personal copy so it can store temporary values
#based on the player's choices. If the player decides to
#reset the values to default, the script can just drop the duplicate.
@onready var Settings = GlobalVariables.get_settings().duplicate();

var active_button = null;

#Node variables#
@onready var command_menu			= self.get_node("CommandMenu");
@onready var back_button				= self.get_node("CommandMenu/BackBtn");
@onready var options_panel			= self.get_node("OptionsPanel");

#Audio options nodes#
@onready var bgm_slider				= self.get_node("OptionsPanel/Audio/Buttons/BGMSlider");
@onready var sfx_slider				= self.get_node("OptionsPanel/Audio/Buttons/SFXSlider");
@onready var mfx_slider				= self.get_node("OptionsPanel/Audio/Buttons/MESlider");
@onready var mute_button				= self.get_node("OptionsPanel/Audio/Buttons/MuteBtn");

#Graphic options nodes#
@onready var window_menu				= self.get_node("OptionsPanel/Graphics/Buttons/WindowModeBtn");
@onready var vsync_button			= self.get_node("OptionsPanel/Graphics/Buttons/VSyncBtn");
@onready var gamma_slider			= self.get_node("OptionsPanel/Graphics/Buttons/GammaSlider");

#Advanced options nodes#
@onready var colour_button			= self.get_node("OptionsPanel/Advanced/Buttons/ColourBtn");
@onready var text_size_slider		= self.get_node("OptionsPanel/Advanced/Buttons/TextSizeSlider");
@onready var letter_spacing_slider	= self.get_node("OptionsPanel/Advanced/Buttons/LetterSpacingSlider");
@onready var odf_button				= self.get_node("OptionsPanel/Advanced/Buttons/ODFBtn");

#We store all the labels in order to update the font when it's necessary
@onready var labels = [
	self.get_node("NextTabLabel"),
	self.get_node("PrevTabLabel"),
	self.get_node("OptionsPanel/Audio/Labels/BGMLabel"),
	self.get_node("OptionsPanel/Audio/Labels/MELabel"),
	self.get_node("OptionsPanel/Audio/Labels/SFXLabel"),
	self.get_node("OptionsPanel/Audio/Labels/MuteLabel"),
	self.get_node("OptionsPanel/Graphics/Labels/WindowLabel"),
	self.get_node("OptionsPanel/Graphics/Labels/VSyncLabel"),
	self.get_node("OptionsPanel/Graphics/Labels/GammaLabel"),
	self.get_node("OptionsPanel/Advanced/Labels/ColourLabel"),
	self.get_node("OptionsPanel/Advanced/Labels/TextSizeLabel"),
	self.get_node("OptionsPanel/Advanced/Labels/LetterSpacingLabel"),
	self.get_node("OptionsPanel/Advanced/Labels/ODFLabel")
	];
#########################

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	self.set_active_button(bgm_slider);
	window_menu.add_item(tr("Windowed"), 0);
	window_menu.add_item(tr("Borderless"), 1);
	window_menu.add_item(tr("Fullscreen"), 2);
	self.update_fonts();
	self.update_buttons();

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta) -> void:
	self.get_input();

#Process the player's input
func get_input() -> void:
	var current_tab = options_panel.get_current_tab();
	if (Input.is_action_just_pressed("ui_prev_tab")):
		SoundManager.play_se("UIMove", false);
		if (current_tab > 0):
			options_panel.set_current_tab(current_tab - 1);
		else:
			options_panel.set_current_tab(options_panel.get_tab_count() - 1);
	elif (Input.is_action_just_pressed("ui_next_tab")):
		SoundManager.play_se("UIMove", false);
		if (current_tab < options_panel.get_tab_count() - 1):
			options_panel.set_current_tab(current_tab + 1);
		else:
			options_panel.set_current_tab(0);
	if (Input.is_action_just_pressed("ui_down") || Input.is_action_just_pressed("ui_up") ||
	Input.is_action_just_pressed("ui_right") || Input.is_action_just_pressed("ui_left")):
		SoundManager.play_se("UIMove", false);

#Sets the currently active button to make it grab the focus
func set_active_button(button : Control) -> void:
	active_button = button;
	active_button.grab_focus();

#Updates the labels and buttons font with the currently in-use font
func update_fonts() -> void:
	for label in self.labels:
		label.set("theme_override_fonts/font", GlobalVariables.get_font_to_use());
	options_panel.set("theme_override_fonts/font", GlobalVariables.get_font_to_use());
	colour_button.set("theme_override_fonts/font", GlobalVariables.get_font_to_use());
	window_menu.set("theme_override_fonts/font", GlobalVariables.get_font_to_use());
	for button in command_menu.get_children():
		button.update_font();

#Updates the values on the button according to the GlobalVariables settings
func update_buttons() -> void:
	bgm_slider.set_value(Settings["BGM"]);
	sfx_slider.set_value(Settings["SFX"]);
	mfx_slider.set_value(Settings["MFX"]);
	mute_button.set_pressed(Settings["MUTE"]);
	vsync_button.set_pressed(Settings["VSYNC"]);
	gamma_slider.set_value(Settings["GAMMA"]);


#########################
#	SIGNALS HANDLING	#
#########################

#Settings Sliders and Buttons Signals#

#When the tab on the options panel changes
func _on_OptionsPanel_tab_changed(tab : int) -> void:
	self.set_active_button(options_panel.get_tab_control(tab).get_node("Buttons").get_children()[0]);

#When the Mute button is toggled
func _on_MuteBtn_toggled(button_pressed : bool) -> void:
	Settings["MUTE"] = button_pressed;
	AudioServer.set_bus_mute(0, button_pressed); #Mutes the Master bus

#When the VSync button is toggled
func _on_VSyncBtn_toggled(button_pressed : bool) -> void:
	Settings["VSYNC"] = button_pressed;
	DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_ENABLED if (button_pressed) else DisplayServer.VSYNC_DISABLED);

#When the user selects the OpenDyslexic Font
func _on_ODFBtn_toggled(button_pressed : bool) -> void:
	Settings["FONT_TO_USE"] = "opendyslexic" if button_pressed else "text";
	GlobalVariables.set_font_to_use(Settings["FONT_TO_USE"]);
	self.update_fonts();

func _on_BGMSlider_value_changed(value : float) -> void:
	Settings["BGM"] = value;
	var bgm_db = (value * 104 / 100) - 80;
	SoundManager.set_bgm_volume_db(bgm_db);

func _on_MESlider_value_changed(value : float) -> void:
	Settings["MFX"] = value;
	var mfx_db = (value * 104 / 100) - 80;
	SoundManager.set_me_volume_db(mfx_db);
	
func _on_SFXSlider_value_changed(value : float) -> void:
	Settings["SFX"] = value;
	var sfx_db = (value * 104 / 100) - 80;
	SoundManager.set_se_volume_db(sfx_db);

func _on_WindowModeBtn_item_selected(ID : int) -> void:
	Settings["WINDOW"] = ID;
	match ID:
		0:
			get_window().borderless = (false);
			get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN if (false) else Window.MODE_WINDOWED;
		1:
			get_window().borderless = (true);
			get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN if (false) else Window.MODE_WINDOWED;
		2:
			get_window().borderless = (false);
			get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN if (true) else Window.MODE_WINDOWED;
	
func _on_GammaSlider_value_changed(value : float) -> void:
	Settings["GAMMA"] = value;
	GammaController.set_gamma(value); #Changes the gamma of the screen in real time


#Reset and Accept Button Signals#

func _on_ResetBtn_pressed() -> void:
	self.Settings = GlobalVariables.get_settings().duplicate(); #Reset the Settings dictionary to the previously saved one
	self.update_buttons();
	GlobalVariables.load_on_engine(); #Resets the engine to the previously saved settings

func _on_AcceptBtn_pressed() -> void:
	GlobalVariables.set_settings(Settings); #Updates global settings
	SaveLoad.save_game(); #Saves the game
	SceneManager.change_scene_to_file(BACK_SCENE);