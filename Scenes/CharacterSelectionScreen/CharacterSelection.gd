extends Control

#constants#
const GO_BACK_SCENE = "res://Scenes/TitleScreen/TitleScreen.tscn";
###########

#variables#
var active_button = null;

@onready var Player_Cursors = [
	self.get_node("PlayerCursors/P1"),
	self.get_node("PlayerCursors/P2"),
	self.get_node("PlayerCursors/P3"),
	self.get_node("PlayerCursors/P4")
	];
	
@onready var Player_Windows = [
	self.get_node("PlayersGrid/P1"),
	self.get_node("PlayersGrid/P2"),
	self.get_node("PlayersGrid/P3"),
	self.get_node("PlayersGrid/P4")
	];
	
@onready var characters_grid = self.get_node("CharacterGrid");
###########

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	self.load_characters();

#Handles players' input
func _input(event : InputEvent) -> void:
	if (event.is_action_pressed("ui_accept")):
		self.add_player(event.get_output_device());
	if (event.is_action_pressed("ui_cancel")):
		if ($PlayersGrid/P1.has_joined()):
			self.remove_player(event.get_output_device());
		else:
			var device = event.get_output_device();
			if (device == 0 || PlayersManager.is_device_known(device)):
				self.go_back();
	
func set_active_button(button : Node) -> void:
	if (active_button != null):
		active_button.release_focus();
	active_button = button;
	active_button.grab_focus();
	GlobalVariables.debug_print(active_button.get_name());
	Player_Cursors[0].set_global_position(active_button.get_global_position());

#Goes back to previous screen
func go_back() -> void:
	SceneManager.change_scene_to_file(GO_BACK_SCENE);

#Loads characters in the grid
func load_characters() -> void:
	var characters_list = GlobalVariables.get_characters_list();
	for character in characters_list:
		var character_button_scene = load("res://UI/Buttons/CharacterButton.tscn");
		var character_button_node = character_button_scene.instantiate();
		character_button_node.set_character(character);
		character_button_node.connect_signals(self);
		character_button_node.update_gui();
		characters_grid.add_child(character_button_node);


#################################
#		PLAYERS HANDLING		#
#################################

#Updates the given player's window
func update_player(player : int) -> void:
	var character : Character = active_button.get_character();
	if (self.Player_Windows[player - 1].has_joined()):
		self.Player_Windows[player - 1].set_character_info(character.get_stats());
		self.Player_Windows[player - 1].set_character_name(character.get_name());

#Removes a player from the screen
func remove_player(device : int) -> void:
	if (PlayersManager.is_device_known(device)):
		var player = PlayersManager.get_player(device);
		if (PlayersManager.remove_player(player)):
			self.Player_Windows[player-1].join(false);
			self.Player_Cursors[player-1].set_visible(false);

#Adds a new player to the screen
func add_player(device : int) -> void:
	var player = PlayersManager.add_player(device);
	if (player < 0):
		return;
	self.Player_Windows[player-1].join(true);
	self.Player_Cursors[player-1].set_visible(true);
	self.set_active_button(characters_grid.get_children()[0]);


#################################
#		SIGNALS HANDLING		#
#################################

func _on_CharacterButton_pressed() -> void:
	pass

func _on_CharacterButton_focus_entered(button : Node) -> void:
	self.set_active_button(button);
	self.update_player(1);

func _on_BackBtn_pressed():
	self.go_back();
