extends Node2D

const SNOWBALL_SCENE = "res://Entities/Snowball/snowball.tscn"

@onready var phantom_camera_2d = $PhantomCamera2D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#phantom_camera_2d.rect


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func throw_snowball(starting_position : Vector2, target_position : Vector2, player : int) -> void:
	var snowball_scene = load(SNOWBALL_SCENE)
	var snowball = snowball_scene.instantiate()
	add_child(snowball)
	snowball.global_position = starting_position
	snowball.motion = target_position
	snowball.set_collision_mask_value(player + 1, false)


func _on_player_snowball_thrown(pos : Vector2, target : Vector2, player : int) -> void:
	throw_snowball(pos, target, player)
